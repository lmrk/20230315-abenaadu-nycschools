package com.example.nyc_schools

import com.example.nyc_schools.model.NycSchoolInfo
import com.example.nyc_schools.network.NycSchoolRepository
import com.example.nyc_schools.ui.NycSchoolsState
import com.example.nyc_schools.ui.NycSchoolsViewModel
import com.example.nyc_schools.util.Resource
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NycSchoolsViewModelTest {

    private lateinit var viewModel: NycSchoolsViewModel
    private lateinit var repository: NycSchoolRepository

    @Before
    fun setUp() {
        repository = mock(NycSchoolRepository::class.java)
        viewModel = NycSchoolsViewModel(repository)
    }

    @Test
    fun `getNycSchools should update state on success`() = runBlocking {
        // Given a valid schoolName and successful repository responses
        val nycSchoolList = listOf(NycSchoolInfo("school1"), NycSchoolInfo("school2"))
        val groupedNycSchoolList = nycSchoolList.sortedBy { it.schoolName }
            .groupBy { it.schoolName?.first() }
        `when`(repository.getNycSchools()).thenReturn(Resource.Success(nycSchoolList))

        // When getNycSchools
        viewModel.getNycSchools()

        // Then state should be updated with the expected values
        assertEquals(viewModel.state.value.nycSchoolList, nycSchoolList)
        assertEquals(viewModel.state.value.initialList, groupedNycSchoolList)
        assertEquals(viewModel.state.value.groupedNycSchoolList, groupedNycSchoolList)
        assertFalse(viewModel.state.value.showLoading)
        assertFalse(viewModel.state.value.showError)
    }

    @Test
    fun `getNycSchools should update state on error`() = runBlocking {
        // Given invalid data
        `when`(repository.getNycSchools()).thenReturn(Resource.Error("Error"))

        // When getNycSchools is called
        viewModel.getNycSchools()

        // Then should update with expected results
        assertEquals(emptyList<NycSchoolInfo>(), viewModel.state.value.nycSchoolList)
        assertTrue(viewModel.state.value.showError)
        assertFalse(viewModel.state.value.showLoading)
        assertTrue(viewModel.state.value.groupedNycSchoolList.isEmpty())
    }

    @Test
    fun `searchSchools should filter the list by query`() {
        // Given a query
        val nycSchoolList = listOf(
            NycSchoolInfo("School A"), NycSchoolInfo("School B"), NycSchoolInfo("School C")
        )
        val groupedNycSchoolList = nycSchoolList.sortedBy { it.schoolName }
            .groupBy { it.schoolName?.first() }
        viewModel._state.value = NycSchoolsState(
            nycSchoolList = nycSchoolList,
            initialList = groupedNycSchoolList,
            groupedNycSchoolList = groupedNycSchoolList
        )

        // When searchSchools is called
        viewModel.searchSchools("A")

        // Then should update with expected results
        assertEquals(viewModel.state.value.groupedNycSchoolList, mapOf('S' to listOf(NycSchoolInfo("School A"))))
        assertTrue(viewModel.state.value.isSearch)
    }
}




