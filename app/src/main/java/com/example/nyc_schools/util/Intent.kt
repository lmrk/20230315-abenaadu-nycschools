package com.example.nyc_schools.util

import android.content.Intent
import android.net.Uri

fun String.asDialIntent(): Intent =
    Intent(Intent.ACTION_DIAL, Uri.parse("tel:$this"))

fun String.asUriIntent(): Intent =
    Intent(Intent.ACTION_VIEW, Uri.parse("https://$this") )

fun locationIntent(lat: String, lng: String, name:String): Intent =
    Intent(Intent.ACTION_VIEW, Uri.parse("geo:$lat,$lng?q=$lng($name)") )