package com.example.nyc_schools.di

import com.example.nyc_schools.network.NycSchoolRepository
import com.example.nyc_schools.network.NycSchoolRepositoryImpl
import com.example.nyc_schools.network.NycSchoolsApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NycRepositoryModule {

    @Singleton
    @Provides
   fun provideNycSchoolRepository (
        apiService: NycSchoolsApi
    ): NycSchoolRepository = NycSchoolRepositoryImpl(apiService)
}