package com.example.nyc_schools.model

import androidx.annotation.StringRes

data class ScoreInfo(
    @StringRes val labelResId: Int,
    val value: String?
)