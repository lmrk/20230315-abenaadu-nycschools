package com.example.nyc_schools.ui.view

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.nyc_schools.ui.nav.DetailsRoute

@Composable
fun NycSchoolApp (
    navController: NavHostController = rememberNavController()
) {
    NavHost(
        navController = navController,
        startDestination = "Nyc_School_List_Screen"
    ){
        composable(
            route = "Nyc_School_List_Screen"
        ) {
            NycSchoolsScreen(
                navController = navController,
                onClick = { dbn, location, website, phone, city, stateCode ->
                    navController.navigate(
                        DetailsRoute.routeDetails(
                            dbn,
                            location,
                            website,
                            phone,
                            city,
                            stateCode
                        )
                    )
                }
            )
        }

        composable(
            route = DetailsRoute.ROUTE_DETAILS,
            arguments = DetailsRoute.detailsArgument
        ){
            DetailsScreen(
                navController = navController
            ){
                navController.popBackStack()
            }

        }

    }

}
