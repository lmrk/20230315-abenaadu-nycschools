package com.example.nyc_schools.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nyc_schools.model.NycSchoolInfo
import com.example.nyc_schools.network.NycSchoolRepository
import com.example.nyc_schools.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NycSchoolsViewModel @Inject constructor(
    private val repository: NycSchoolRepository
) : ViewModel() {

    val _state = MutableStateFlow(NycSchoolsState())
    val state: StateFlow<NycSchoolsState> = _state.asStateFlow()

    init {
        getNycSchools()
    }

    fun getNycSchools() {
        viewModelScope.launch {
            _state.value = _state.value.copy(showLoading = true)
            val schoolResult = repository.getNycSchools()
            when (schoolResult) {
                is Resource.Success -> {
                    val nycSchoolList = schoolResult.data?.toMutableList() ?: emptyList()
                    val groupedNycSchoolList = nycSchoolList.sortedBy { it.schoolName }
                        .groupBy { it.schoolName?.first() }
                    _state.value = _state.value.copy(
                        showLoading = false,
                        nycSchoolList = nycSchoolList,
                        initialList = groupedNycSchoolList,
                        groupedNycSchoolList = groupedNycSchoolList
                    )
                }
                is Resource.Error -> {
                    _state.value = _state.value.copy(
                        nycSchoolList = emptyList(),
                        initialList = emptyMap(),
                        groupedNycSchoolList = emptyMap(),
                        showLoading = false,
                        showError = true
                    )
                }
                else -> {}
            }
        }
    }

    fun searchSchools(query: String) {
        val searchList = state.value.groupedNycSchoolList
        if (query.isEmpty()) {
            _state.value = state.value.copy(
                isSearch = false,
                groupedNycSchoolList = searchList
            )
        }
        _state.value = state.value.copy(isSearch = true)
        val filteredMap = mutableMapOf<Char?, List<NycSchoolInfo>>()
        searchList.forEach { (key, value) ->
            val filteredSchool = value.filter { school ->
                school.schoolName?.contains(query.trim(), ignoreCase = true) ?: false
            }
            if (filteredSchool.isNotEmpty()) {
                filteredMap[key] = filteredSchool
            }
        }
        _state.value = state.value.copy(groupedNycSchoolList = filteredMap)
    }
}

data class NycSchoolsState(
    val initialList: Map<Char?, List<NycSchoolInfo>> = emptyMap(),
    val nycSchoolList: List<NycSchoolInfo> = emptyList(),
    val groupedNycSchoolList: Map<Char?, List<NycSchoolInfo>> = emptyMap(),
    val showLoading: Boolean = false,
    val showError: Boolean = false,
    val isSearch: Boolean = false
)
