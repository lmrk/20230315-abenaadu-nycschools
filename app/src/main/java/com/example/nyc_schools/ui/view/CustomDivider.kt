package com.example.nyc_schools.ui.view

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun CustomDivider(){
    Divider(
        modifier = Modifier.padding(vertical = 5.dp),
        color = Color.LightGray,
        thickness = 0.8.dp
    )
}
