package com.example.nyc_schools.ui

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nyc_schools.model.NycSchoolInfo
import com.example.nyc_schools.model.SATScoreInfo
import com.example.nyc_schools.network.NycSchoolRepository
import com.example.nyc_schools.util.Resource
import com.example.nyc_schools.ui.nav.DetailsRoute
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel  @Inject constructor(
    private val repository: NycSchoolRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _state = MutableStateFlow(DetailsState())
    val state: StateFlow<DetailsState> = _state.asStateFlow()

    private val dbn = savedStateHandle.getStateFlow(DetailsRoute.DBN, "")

    init {
        getData(dbn.value)
    }

    fun getData(dbn: String) {
        viewModelScope.launch {
            _state.value = _state.value.copy(showLoading = true)
            val getSchool = async {
                repository.getNycSchool(dbn)
            }
            val getScores = async {
                repository.getScore(dbn)
            }
            val schoolResult = getSchool.await()
            val scoreResult = getScores.await()

            when {
                schoolResult is Resource.Success && scoreResult is Resource.Success -> {
                    val schoolInfo = schoolResult.data?.toNycSchoolInfo() ?: NycSchoolInfo()
                    val scores = scoreResult.data?.toSATScoreInfoList() ?: SATScoreInfo()
                    _state.value = _state.value.copy(
                        showLoading = false,
                        schoolInfo = schoolInfo,
                        score = scores
                    )
                }
                schoolResult is Resource.Error && scoreResult is Resource.Error -> {
                    _state.value = _state.value.copy(showLoading = true)
                }
            }

        }
    }
}

data class DetailsState(
    val score: SATScoreInfo = SATScoreInfo(),
    val schoolInfo: NycSchoolInfo = NycSchoolInfo(),
    val showLoading: Boolean = false,
    val showError: Boolean = false,
)

fun Array<NycSchoolInfo>.toNycSchoolInfo(): NycSchoolInfo {
    if (isEmpty()) return NycSchoolInfo()
    return this[0]
}

fun Array<SATScoreInfo>.toSATScoreInfoList(): SATScoreInfo {
    if (isEmpty()) return SATScoreInfo()
    return this[0]
}